<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Main page
Route::get('/', 'PagesController@home')->name('home');

// Show upload forms
Route::get('/upload', 'PagesController@upload');

// Store uploaded images
Route::post('/upload', 'ImagesController@storeImage');
//Route::post('/upload/watermark', 'ImagesController@storeWatermark');

// Create and store final image with text-watermark
Route::post('/create/text', 'ImagesController@createWatermarkText');

// Show cropper
Route::get('/crop', function(){
    return view('cropper');
});
// Create and store final image with image-watermark
Route::post('/create/image', 'ImagesController@createWatermarkImage');


Route::get('/test', 'ImagesController@test');


//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
