/* Main image*/
Dropzone.options.image = {
    paramName: "image",
    init: function() {
        // success
        this.on("success", function(file, response) {
            swal({
                title: "Success!",
                text: response.message,
                timer: 2000,
                showConfirmButton: false,
                type: 'success'
            });
        });
        // error
        this.on("error", function(file, response){
            swal({
                title: "Oops!",
                text: response.message,
                showConfirmButton: true,
                type: 'error'
            });
        });
    }


};

/* Watermark */
Dropzone.options.watermark = {
    paramName: "watermark",
    init: function() {
        this.on("success", function(file) {
            $(location).attr('href', '/crop').delay(2000);
        });
    }
};




$(document).ready(function(){

    /* Select watermark*/

    $('#choose').click(function(){
        var $sel = $('#choose').val();

        if($sel == 'image'){
             $('#watermark').toggleClass('hidden');

            $("form#image").toggleClass('hidden');


        }else if($sel == 'text'){
            $('#text, #send').toggleClass('hidden');

        }
    });

    /* Send text*/

    var form = $('#text');
    form.submit(function(e) {
        e.preventDefault();
        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: form.serialize(),
            dataType: 'json'
        })
            .done(function(response){
                if (response.success) {
                    swal({
                        title: "Success!",
                        text: response.success,
                        timer: 4000,
                        showConfirmButton: false,
                        type: 'success'
                    });
                     window.location.replace('/');
                } else {
                    swal({
                        title: "Oops!",
                        text: response.errors,
                        showConfirmButton: true,
                        type: 'error'
                    });

                }
            })
            .fail(function(){
                swal("Oops!", "Something wrong", 'error');
            });

    });

});