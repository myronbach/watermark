$(function(){
    'use strict';

    var $image = $('#img');
    $image.cropper();

    var croppingData = {};
    $('#crop-btn').click(function(){
        croppingData = $image.cropper('getCroppedCanvas');
        //$('.image-data').html(croppingData);
        $('#cropped-image').val(croppingData.toDataURL());
    });


});
