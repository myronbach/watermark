<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Validator;
use UxWeb\SweetAlert\SweetAlert;


class ImagesController extends Controller
{

    public function storeImage(Request $request)
    {
        $filename = key($_FILES); // 'image' & 'watermark'
        $file = Input::file($filename);
        $json = [];

        $size = $file->getSize();
        $extension = 'png';
        $destination = 'images';
        $fullName = $filename . '.' . $extension;  // image.jpg
        $pathToFile = public_path() . '/' . $destination . '/' . $fullName; //  images/image.png

        $img = Image::make($file)->encode('png');

        if ($filename == 'image') {
                $success = $img->fit(960, 440, function ($constrain) {
                    $constrain->upsize();
                })->save($pathToFile);
        } else {
                $success = $img->save($pathToFile);
        }


        if ($success) {
            $json = [
                'name' => $filename,
                'size' => $size,
                'url' => $pathToFile,
                'message' => 'Uploaded successfully!',
            ];
            return Response::json($json, 200);
        } else {
            $json = [
                'message' => 'Error uploading images',
            ];
            return Response::json($json, 202);
        }
    }

    public function createWatermarkText(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'text' => 'filled'
        ]);
        if ($validator->fails()) {
            $message = ['errors' => $validator->messages()->all()];
            return Response::json($message, 202);
        } else {
            $text = $request->get('text');

            $pathToImage = public_path() . '/images/image.png';
            $img = Image::make($pathToImage);
            $bright = $this->getBright($pathToImage, 255);
            $color = $this->getWatermarkColor($bright, 0.6);
            $pos_x = $img->width() / 2;
            $pos_y = $img->height() / 2;
            $fontSize = round(($img->height() / 2) / 1.33);

            $img->text($text, $pos_x, $pos_y, function ($font) use ($color, $fontSize) {
                $font->file('fonts/Sansation_Bold.ttf');
                $font->size($fontSize);
                $font->color($color);
                $font->align('center');
                $font->valign('center');
            })->save(public_path() . '/images/done.png');

            $message = ['success' => 'Watermark Has been created!'];
            return Response::json($message, 200);
        }

    }



    protected function getWatermarkColor($brightImg, $opacity = 0.6)
    {
        $diff = 120;
        if($brightImg < 128){
            $color = $brightImg + $diff;
        }else{
            $color = $brightImg - $diff;
        }
        //$color = ($brightImg + $diff)%255;


        return [$color, $color, $color, $opacity];
    }

    public function getWartermarkBright($brightImg, $brightWater)
    {
        $diff =120;

        if($brightImg <= 128){
            $bright = $brightImg + $diff;
        }else{
            $bright = $brightImg - $diff;
        }
        //$bright = ($brightImg - $diff)%255;

        $brightOut =  $bright - $brightWater;

        /*dump('Calc BrightWater: ' .$bright);
        dump('Bright wat out: ' .$brightOut);
        dd();*/

        return $brightOut;
    }

    public function test()
    {
        //$path = public_path() . '/images/image.png';
        //$water = public_path() . '/images/watermark.png';
        //$gray = Image::canvas(760, 440, '#7f7f7f')->save(public_path().'/images/test1.png');
        //$white = Image::canvas(760, 440, '#ffffff')->save(public_path().'/images/test2.png');
        //$black = Image::canvas(760, 440, '#00000')->save(public_path().'/images/test3.png');
        /*$img = Image::make($path);
        $img->fit(800, 600, function ($constrain) {
            $constrain->upsize();
        });
        $watermark = Image::make($water)->brightness(-50);

        $img->insert($watermark, 'center');

        return $img->response();*/

        $pathToImage = public_path() . '/images/image.png';
        $pathToWatermark = public_path() . '/images/test.png';

        $img = imagecreatefrompng($pathToImage);
        $watermark = imagecreatefrompng($pathToWatermark);

        // size of the image
        $img_x = imagesx($img);
        $img_y = imagesy($img);

        // size of the watermark
        $wat_x = imagesx($watermark);
        $wat_y = imagesy($watermark);

        // padding bottom right
        $pad_right =20;
        $pad_bottom = 20;

        // coordinate
        $point_x = $img_x - ($wat_x + $pad_right);
        $point_y = $img_y - ($wat_y + $pad_bottom);


       /* dump($wat_x);
        dump($wat_y);
        dump($point_x);
        dump($point_y);
        dd();*/

        $brightImg = $this->getBright($pathToImage);
        $brightWat = $this->getBright($pathToWatermark);
        /*dump('Water '.$brightWat);
        dump('Img '. $brightImg);*/
        $brightness = $this->getWartermarkBright($brightImg, $brightWat);

        imagefilter($watermark, IMG_FILTER_BRIGHTNESS, $brightness);
        imagecopymerge($img, $watermark, $point_x, $point_y, 0, 0,$wat_x,$wat_y, 50);


        header('Content-type: image/png');
        imagepng($img);
        imagedestroy($img);


    }

    protected function getBright($filename, $num_samples = 10)
    {
        $img = imagecreatefrompng($filename);

        $width = imagesx($img);
        $height = imagesy($img);

        $x_step = intval($width / $num_samples);
        $y_step = intval($height / $num_samples);

        $total_lum = 0;

        $sample_no = 1;

        for ($x = 0; $x < $width; $x += $x_step) {
            for ($y = 0; $y < $height; $y += $y_step) {

                $rgb = imagecolorat($img, $x, $y);
                $r = ($rgb >> 16) & 0xFF;
                $g = ($rgb >> 8) & 0xFF;
                $b = $rgb & 0xFF;

                // choose a simple luminance formula from here
                // http://stackoverflow.com/questions/596216/formula-to-determine-brightness-of-rgb-color
                $lum = ($r + $r + $b + $g + $g + $g) / 6;

                $total_lum += $lum;

                $sample_no++;
            }
        }

        // work out the average
        $avg_lum = $total_lum / $sample_no;

        return intval($avg_lum);
    }

    public function createWatermarkImage()
    {
        $files = Input::all();

        $file = $files['cropped-image'];
        $pathToImage = public_path() . '/images/image.png';
        $pathToWatermark = public_path() . '/images/watermark.png';
        $pathToSave = public_path() . '/images/done.png';

        $success = Image::make($file)->save($pathToWatermark);
        if (!$success){
            SweetAlert::error('Error uploading image', 'Error')->autoclose(2000);
            return redirect()->back();
        }

        $img = imagecreatefrompng($pathToImage);
        $watermark = imagecreatefrompng($pathToWatermark);

        // size of the image
        $img_x = imagesx($img);
        $img_y = imagesy($img);

        // size of the watermark
        $wat_x = imagesx($watermark);
        $wat_y = imagesy($watermark);

        // padding bottom right
        $pad_right =20;
        $pad_bottom = 20;

        // coordinate
        $point_x = $img_x - ($wat_x + $pad_right);
        $point_y = $img_y - ($wat_y + $pad_bottom);


        $brightImg = $this->getBright($pathToImage);
        $brightWat = $this->getBright($pathToWatermark);
        /*dump('Water '.$brightWat);
        dump('Img '. $brightImg);*/
        $brightness = $this->getWartermarkBright($brightImg, $brightWat);

        imagefilter($watermark, IMG_FILTER_BRIGHTNESS, $brightness);
        imagecopymerge($img, $watermark, $point_x, $point_y, 0, 0,$wat_x,$wat_y, 50);
        $success = imagepng($img, $pathToSave);

        if ($success) {
            SweetAlert::success('Watermark has been created!', 'Success!')->autoclose(2000);
            return redirect('/');
        } else {
            SweetAlert::error('There is an error', 'Error')->autoclose(2000);

        }


    }

}
