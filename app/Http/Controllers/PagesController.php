<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function home()
    {
        //dump("Hello");

        return view('index');
    }

    public function upload()
    {
        return view('upload');
    }
}
