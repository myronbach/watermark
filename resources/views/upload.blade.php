@extends('layouts.main')
@section('title', 'Home')

@section('content')



    <div class="container spark-screen">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Create watermark protection</div>

                <div class="panel-body">

                    @include('shared.errors')
                    @include('shared.status')


                    <div class="panel-body">

                        <form action="/upload" method="post" class="dropzone " id="image"
                              enctype="multipart/form-data">
                            <input type="hidden" name="type" value="">
                            <legend class="upload">Select an image</legend>
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <div class="fallback">
                                    <input id="input-image" type="file" name="image">
                                </div>
                            </div>
                        </form>

                        <div class="panel-body ">
                            <form action="/upload" method="post" class="dropzone hidden" id="watermark"
                                  enctype="multipart/form-data">
                                <legend>Select a watermark</legend>
                                {!! csrf_field() !!}
                                <div class="fallback">
                                    <input type="file" name="watermark">
                                </div>
                            </form>
                        </div>

                    </div>
                    <div class="panel-body  choose">
                        <form class="form-horizontal">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label for="choose" class="col-md-4 control-label">Choose a watermark</label>
                                <div class="col-md-6">
                                    <select class="form-control " name="watermark" id="choose">
                                        <option value="noimage" selected="true">no watermark</option>
                                        <option value="image">image</option>
                                        <option value="text">text</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                        {{--<button type="submit" class="btn btn-default">Upload</button>--}}
                        <form action="/create/text" method="post" class="form-horizontal hidden" id="text">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label for="title" class="col-md-4 control-label">Enter some text</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="text">
                                </div>
                                <button type="submit" class="btn btn-primary ladda-button" data-style="expand-left"
                                        data-size="s" data-color="blue">Create
                                </button>
                            </div>
                        </form>





                    </div>


                </div>
            </div>
        </div>
    </div>

@endsection

