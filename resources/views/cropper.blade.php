@extends('layouts.main')
@section('title', 'Cropper')

@section('content')



    <div class="container spark-screen">
        <div class="content col-md-10 col-md-offset-1">

            <div class="title">Crop watermark</div>
            <form method="post" action="/create/image" enctype="multipart/form-data" >
                {!! csrf_field() !!}
                <input type="hidden" id="cropped-image" name="cropped-image" value="">


            <button type="submit" id="crop-btn" class="btn btn-primary">
                Crop image
            </button>
            </form>
            <div class="image-data"></div>

            <div class="img-container">
                <img id="img" src="/images/watermark.png" alt="">
            </div>


        </div>
    </div>

@endsection

