@extends('layouts.main')
@section('title', 'Home')

@section('content')



    <div class="container spark-screen">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                {{--<div class="panel-heading"></div>--}}
                <div><img src="/images/done.png ? {{ time() }}" width="100%" alt=""></div>
                <div class="panel-body" id="app">

                        @include('shared.errors')
                        @include('shared.status')


                </div>
            </div>
        </div>
    </div>

@endsection

