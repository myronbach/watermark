<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    {{--<meta http-equiv="Cache-Control" content="no-cache">--}}
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>@yield('title')</title>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Bootstrap -->
    <link href="css/cropper.min.css" rel="stylesheet">
    <link href="css/app.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" href="css/ladda.min.css">
    <link rel="stylesheet" href="css/ladda-themeless.min.css">
    <link rel="stylesheet" href="/css/sweetalert2.min.css">
    <link rel="stylesheet" href="/css/basic.min.css">
    <link rel="stylesheet" href="/css/dropzone.min.css">
    <link rel="stylesheet" href="css/parsley.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="/js/dropzone.js"></script>




<body>


@include('shared.navbar')

@yield('content')

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
{{--<script src="js/app.js"></script>--}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="/js/spin.min.js"></script>
<script src="/js/ladda.min.js"></script>
{{--<script src="/js/dropzone.js"></script>--}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.3.5/parsley.min.js"></script>

<script src="/js/sweetalert2.min.js"></script>
<script src="/js/cropper.min.js"></script>
<script src="/js/cropper.js"></script>
<script src="/js/custom_script.js"></script>
@include('vendor.sweet.alert')
<!-- Include all compiled plugins (below), or include individual files as needed -->
</body>
</html>